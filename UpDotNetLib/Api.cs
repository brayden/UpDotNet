﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UpDotNetLib.Models.Response;

namespace UpDotNetLib
{
    public class Api
    {
        private readonly string _apiToken;
        
        /// <summary>
        /// Up API
        /// </summary>
        /// <param name="upApiToken">Up API token as string</param>
        public Api(string upApiToken)
        {
            _apiToken = upApiToken;
        }

        /// <summary>
        /// Checks if your API token is correct
        /// </summary>
        /// <returns>Status emoji</returns>
        public async Task<PingResponseModel> PingApi()
        {
            var response = await Http.HttpGet(new Uri("https://api.up.com.au/api/v1/util/ping"), _apiToken);
            return JsonConvert.DeserializeObject<PingResponseModel>(response);
        }

        /// <summary>
        /// Gets a list of accounts limited by pageSize
        /// </summary>
        /// <param name="pageSize">Size of the page, defaults to 10</param>
        /// <returns></returns>
        public async Task<AccountListResponseModel> GetAccountList(int pageSize = 10)
        {
            var response = await Http.HttpGet(new Uri($"https://api.up.com.au/api/v1/accounts?page[size]={pageSize}"), _apiToken);
            return JsonConvert.DeserializeObject<AccountListResponseModel>(response);
        }

        /// <summary>
        /// Gets the next page using the URL from the "Next" property
        /// </summary>
        /// <param name="nextPage">Pass in the URI from "Next"</param>
        /// <returns></returns>
        public async Task<AccountListResponseModel> GetAccountList(Uri nextPage)
        {
            var response = await Http.HttpGet(nextPage, _apiToken);
            return JsonConvert.DeserializeObject<AccountListResponseModel>(response);
        }

        public async Task<TransactionListResponseModel> GetTransactionList(string accountId = null, int pageSize = 50, Dictionary<string, string> filters = null)
        {
            string url = "https://api.up.com.au/api/v1/transactions";
            if (accountId != null)
            {
                url = $"https://api.up.com.au/api/v1/accounts/{accountId}/transactions";
            }

            url += $"?page[size]={pageSize}";

            if (filters != null)
            {
                url = filters.Aggregate(url, (current, filter) => current + $"&filter[{filter.Key}]={filter.Value}");
            }

            var response = await Http.HttpGet(new Uri(url), _apiToken);

            return JsonConvert.DeserializeObject<TransactionListResponseModel>(response);
        }

        public async Task<TransactionListResponseModel> GetTransactionListPage(Uri pageUri)
        {
            var response = await Http.HttpGet(pageUri, _apiToken);

            return JsonConvert.DeserializeObject<TransactionListResponseModel>(response);
        }

        public async Task<TagListResponseModel> GetTagList(int pageSize = 50)
        {
            var uri = new Uri($"https://api.up.com.au/api/v1/tags?page[size]={pageSize}");
            
            var response = await Http.HttpGet(uri, _apiToken);

            return JsonConvert.DeserializeObject<TagListResponseModel>(response);
        }
    }
}