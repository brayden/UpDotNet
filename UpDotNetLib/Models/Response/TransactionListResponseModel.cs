﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UpDotNetLib.Models.Response
{
    public class TransactionListResponseModel
    {
        [JsonProperty("data")]
        public List<TransactionDatum> Data { get; set; }

        [JsonProperty("links")]
        public TransactionListResponseModelLinks Links { get; set; }
    }

    public class TransactionDatum
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("attributes")]
        public TransactionAttributes Attributes { get; set; }

        [JsonProperty("relationships")]
        public TransactionRelationships Relationships { get; set; }

        [JsonProperty("links")]
        public TransactionDatumLinks Links { get; set; }
    }

    public class TransactionAttributes
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("rawText")]
        public string RawText { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("message")]
        public object Message { get; set; }

        [JsonProperty("holdInfo")]
        public HoldInfo HoldInfo { get; set; }

        [JsonProperty("roundUp")]
        public object RoundUp { get; set; }

        [JsonProperty("cashback")]
        public object Cashback { get; set; }

        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("foreignAmount")]
        public object ForeignAmount { get; set; }

        [JsonProperty("settledAt")]
        public DateTime? SettledAt { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }
    }

    public class Amount
    {
        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("valueInBaseUnits")]
        public long ValueInBaseUnits { get; set; }
    }

    public class HoldInfo
    {
        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("foreignAmount")]
        public object ForeignAmount { get; set; }
    }

    public class TransactionDatumLinks
    {
        [JsonProperty("self")]
        public Uri Self { get; set; }
    }

    public class TransactionRelationships
    {
        [JsonProperty("account")]
        public Account Account { get; set; }

        [JsonProperty("transferAccount")]
        public TransferAccount TransferAccount { get; set; }

        [JsonProperty("category")]
        public Account Category { get; set; }

        [JsonProperty("parentCategory")]
        public Account ParentCategory { get; set; }

        [JsonProperty("tags")]
        public Tags Tags { get; set; }
    }

    public class Account
    {
        [JsonProperty("data")]
        public Data Data { get; set; }

        [JsonProperty("links")]
        public AccountLinks Links { get; set; }
    }

    public class Data
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class AccountLinks
    {
        [JsonProperty("related")]
        public Uri Related { get; set; }
    }

    public class Tags
    {
        [JsonProperty("data")]
        public List<object> Data { get; set; }

        [JsonProperty("links")]
        public TransactionDatumLinks Links { get; set; }
    }

    public class TransferAccount
    {
        [JsonProperty("data")]
        public object Data { get; set; }
    }

    public class TransactionListResponseModelLinks
    {
        [JsonProperty("prev")]
        public object Prev { get; set; }

        [JsonProperty("next")]
        public object Next { get; set; }
    }
}