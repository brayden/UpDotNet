﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UpDotNetLib.Models.Response
{
    public class AccountListResponseModel
    {
        [JsonProperty("data")]
        public List<Datum> Data { get; set; }

        [JsonProperty("links")]
        public AccountListResponseModelLinks Links { get; set; }
    }

    public class Datum
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }

        [JsonProperty("relationships")]
        public Relationships Relationships { get; set; }

        [JsonProperty("links")]
        public DatumLinks Links { get; set; }
    }

    public class Attributes
    {
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("balance")]
        public Balance Balance { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }
    }

    public class Balance
    {
        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("valueInBaseUnits")]
        public long ValueInBaseUnits { get; set; }
    }

    public class DatumLinks
    {
        [JsonProperty("self")]
        public Uri Self { get; set; }
    }

    public class Relationships
    {
        [JsonProperty("transactions")]
        public Transactions Transactions { get; set; }
    }

    public class Transactions
    {
        [JsonProperty("links")]
        public TransactionsLinks Links { get; set; }
    }

    public class TransactionsLinks
    {
        [JsonProperty("related")]
        public Uri Related { get; set; }
    }

    public class AccountListResponseModelLinks
    {
        [JsonProperty("prev")]
        public Uri Prev { get; set; }

        [JsonProperty("next")]
        public Uri Next { get; set; }
    }
}