﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UpDotNetLib.Models.Response
{
    public class TagListResponseModel
    {
        [JsonProperty("data")]
        public List<Datum> Data { get; set; }

        [JsonProperty("links")]
        public TagListResponseModelLinks Links { get; set; }
    }

    public class TagDatum
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("relationships")]
        public TagRelationships Relationships { get; set; }
    }

    public class TagRelationships
    {
        [JsonProperty("transactions")]
        public TagTransactions Transactions { get; set; }
    }

    public class TagTransactions
    {
        [JsonProperty("links")]
        public TagTransactionsLinks Links { get; set; }
    }

    public class TagTransactionsLinks
    {
        [JsonProperty("related")]
        public Uri Related { get; set; }
    }

    public class TagListResponseModelLinks
    {
        [JsonProperty("prev")]
        public object Prev { get; set; }

        [JsonProperty("next")]
        public object Next { get; set; }
    }
}