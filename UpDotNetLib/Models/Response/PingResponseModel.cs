﻿using System;
using Newtonsoft.Json;

namespace UpDotNetLib.Models.Response
{
    public class PingResponseModel
    {
        [JsonProperty("meta")]
        public Meta Meta { get; set; }
    }

    public class Meta
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("statusEmoji")]
        public string StatusEmoji { get; set; }
    }
}