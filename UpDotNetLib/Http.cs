﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace UpDotNetLib
{
    public static class Http
    {
        internal static string UserAgent = "UpDotNetLib/1.0";
        
        internal static async Task<string> HttpGet(Uri uri, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.UserAgent.Clear();
                client.DefaultRequestHeaders.UserAgent.TryParseAdd(UserAgent);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }

                throw new HttpException($"Bad HTTP response code: {response.StatusCode}", response.StatusCode);
            }
        }

        internal static async Task<string> HttpPost(Uri uri, string token, string body)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.UserAgent.Clear();
                client.DefaultRequestHeaders.UserAgent.TryParseAdd(UserAgent);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage response = await client.PostAsync(uri, new StringContent(body));
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }

                throw new HttpException($"Bad HTTP response code: {response.StatusCode}", response.StatusCode);
            }
        }
        
        public class HttpException : Exception
        {
            public HttpStatusCode Code;

            public HttpException(string message, HttpStatusCode code) : base(message)
            {
                Code = code;
            }
        }
    }
}