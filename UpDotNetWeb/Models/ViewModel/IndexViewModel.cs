﻿using UpDotNetWeb.Models.DbModel;

namespace UpDotNetWeb.Models.ViewModel
{
    public class IndexViewModel
    {
        public UserDbModel User { get; set; }
    }
}