﻿using UpDotNetWeb.Models.DbModel;

namespace UpDotNetWeb.Models.ViewModel
{
    public class UserSettingsViewModel
    {
        public UserDbModel User { get; set; }
        public bool TokenChecked { get; set; }
        public bool CheckResult { get; set; } = false;
        public string Message { get; set; } = string.Empty;
    }
}