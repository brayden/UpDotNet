﻿using UpDotNetLib.Models.Response;
using UpDotNetWeb.Models.DbModel;

namespace UpDotNetWeb.Models.ViewModel
{
    public class AccountListViewModel
    {
        public UserDbModel User { get; set; }
        public AccountListResponseModel Accounts { get; set; }
    }
}