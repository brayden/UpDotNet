﻿using UpDotNetLib.Models.Response;
using UpDotNetWeb.Models.DbModel;

namespace UpDotNetWeb.Models.ViewModel
{
    public class TransactionListViewModel
    {
        public UserDbModel User { get; set; }
        public TransactionListResponseModel Transactions { get; set; }
    }
}