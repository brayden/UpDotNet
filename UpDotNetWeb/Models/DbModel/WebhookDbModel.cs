﻿namespace UpDotNetWeb.Models.DbModel
{
    public class WebhookDbModel
    {
        public int Id { get; set; }
        public UserDbModel User { get; set; }
        public string UpWebhookId { get; set; }
        public string UpSecretKey { get; set; }
    }
}