﻿using System;

namespace UpDotNetWeb.Models.DbModel
{
    public class AuditLogDbModel
    {
        public string Id { get; set; }
        public UserDbModel User { get; set; }
        public string Operation { get; set; }
        public string Arguments { get; set; }
        public string UserAgent { get; set; }
        public string IpAddress { get; set; }
        public DateTimeOffset Timestamp { get; set; }
    }
}