﻿namespace UpDotNetWeb.Models.DbModel
{
    public class SettingDbModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}