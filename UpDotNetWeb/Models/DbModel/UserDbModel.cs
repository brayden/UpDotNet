﻿namespace UpDotNetWeb.Models.DbModel
{
    public class UserDbModel
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UpApiKey { get; set; }
    }
}