﻿namespace UpDotNetWeb.Models.DbModel
{
    public class NotificationTargetDbModel
    {
        public int Id { get; set; }
        public UserDbModel User { get; set; }
        public bool Enabled { get; set; }
        public bool TriggerOnPing { get; set; }
        public bool TriggerOnTransactionCreated { get; set; }
        public bool TriggerOnTransactionSettled { get; set; }
        public bool TriggerOnTransactionDeleted { get; set; }
        // Kinda vague but basically encode whatever you need to deliver the notification
        public string Target { get; set; }
        // Type of notification, SMTP, Telegram, whatever
        public string Type { get; set; }
        public WebhookDbModel Webhook { get; set; }
    }
}