﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;
using UpDotNetLib;
using UpDotNetWeb.Contexts;
using UpDotNetWeb.Models.ViewModel;

namespace UpDotNetWeb.Controllers
{
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public TransactionController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IActionResult> Index(string accountId = null)
        {
            var guid = User.GetObjectId();
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);
            var transactions = await new Api(user.UpApiKey).GetTransactionList(accountId);
            return View(new TransactionListViewModel {User = user, Transactions = transactions});
        }
        
        public async Task<IActionResult> Page(string pageUrl)
        {
            var guid = User.GetObjectId();
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);
            var pageUri = new Uri(pageUrl);
            // Theoretically it'd be possible to craft a request and reveal the API token to a third party
            // Though API keys aren't really secure but maybe this'll change in the future
            if (pageUri.Host != "api.up.com.au")
            {
                return BadRequest("pageUrl doesn't appear to be an Up API URL");
            }
            var transactions = await new Api(user.UpApiKey).GetTransactionListPage(pageUri);
            return View("Index", new TransactionListViewModel {User = user, Transactions = transactions});
        }
    }
}