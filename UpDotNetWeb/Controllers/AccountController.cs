﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;
using UpDotNetLib;
using UpDotNetWeb.Contexts;
using UpDotNetWeb.Models.ViewModel;

namespace UpDotNetWeb.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public AccountController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            var guid = User.GetObjectId();
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);
            var accounts = await new Api(user.UpApiKey).GetAccountList();
            return View(new AccountListViewModel {User = user, Accounts = accounts});
        }
    }
}