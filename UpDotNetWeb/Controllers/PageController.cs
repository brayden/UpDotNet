﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;
using UpDotNetLib;
using UpDotNetLib.Models.Response;
using UpDotNetWeb.Contexts;
using UpDotNetWeb.Models.DbModel;
using UpDotNetWeb.Models.ViewModel;

namespace UpDotNetWeb.Controllers
{
    public class PageController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public PageController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var guid = User.GetObjectId();

            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);
            if (user == null)
            {
                user = (await _dbContext.Users.AddAsync(new UserDbModel
                {
                    Guid = guid
                })).Entity;
            }
            await _dbContext.SaveChangesAsync();
            
            return View(new IndexViewModel {User = user});
        }

        [Authorize]
        public async Task<IActionResult> UserSettings()
        {
            var guid = User.GetObjectId();
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);

            return View(new UserSettingsViewModel {User = user, TokenChecked = false});
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> UserSettings(string upApiKey)
        {
            var api = new Api(upApiKey);
            var guid = User.GetObjectId();
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Guid == guid);
            PingResponseModel response;
            try
            {
                response = await api.PingApi();
            }
            catch (Http.HttpException e)
            {
                return View(new UserSettingsViewModel
                {
                    User = user,
                    CheckResult = false,
                    TokenChecked = true,
                    Message = e.Message
                });
            }
            user.UpApiKey = upApiKey;
            await _dbContext.SaveChangesAsync();
            return View(new UserSettingsViewModel
                {User = user, TokenChecked = true, CheckResult = true, Message = response.Meta.StatusEmoji});
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}