﻿using Microsoft.EntityFrameworkCore;
using UpDotNetWeb.Models;
using UpDotNetWeb.Models.DbModel;

namespace UpDotNetWeb.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {}
        
        public DbSet<UserDbModel> Users { get; set; }
        public DbSet<SettingDbModel> Settings { get; set; }
        public DbSet<AuditLogDbModel> AuditLogs { get; set; }
        public DbSet<WebhookDbModel> Webhooks { get; set; }
        public DbSet<NotificationTargetDbModel> NotificationTargets { get; set; }
    }
}